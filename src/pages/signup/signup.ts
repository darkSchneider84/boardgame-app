import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import {Http, Headers, RequestOptions} from '@angular/http';
import { TermsOfServicePage } from '../terms-of-service/terms-of-service';
import { PrivacyPolicyPage } from '../privacy-policy/privacy-policy';
import { LoginPage } from '../login/login';
import { TabsNavigationPage } from '../tabs-navigation/tabs-navigation';

@Component({
  selector: 'signup-page',
  templateUrl: 'signup.html'
})
export class SignupPage {
  signup: FormGroup;
  main_page: { component: any };

  constructor(public nav: NavController, public modal: ModalController, private http:Http) {
    this.main_page = { component: TabsNavigationPage };

    this.signup = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('test', Validators.required),
      confirm_password: new FormControl('test', Validators.required)
    });
  }

  doSignup(){
    console.log(this.signup.value);
    
    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    let data_string = {
       'data': {
           'attributes': {
               'mail': this.signup.value.email
           }
       }
    };

    let data = JSON.stringify(data_string);


    return this.http.post('https://boardgametournament.beebeeboard.com/api/v1/user_registration_request',data, options)
    .toPromise()
    .then(response => {  
      console.log(response); 
      let imgWr: HTMLElement = document.getElementById('wrongmail');
      imgWr.style.display = 'none';
      let imgEl: HTMLElement = document.getElementById('forgotmessage');
      imgEl.style.display = 'block';
    },this.handleError);

  }

  handleError(error) {
      let imgWr: HTMLElement = document.getElementById('wrongmail');
      imgWr.style.display = 'block';
      let imgEl: HTMLElement = document.getElementById('forgotmessage');
      imgEl.style.display = 'none';
  }

   goToSignup() {
    this.nav.push(SignupPage);
  }

  goToLogin() {
    this.nav.push(LoginPage);
  }

  doFacebookSignup() {
    this.nav.setRoot(this.main_page.component);
  }

  doGoogleSignup() {
    this.nav.setRoot(this.main_page.component);
  }

  showTermsModal() {
    let modal = this.modal.create(TermsOfServicePage);
    modal.present();
  }

  showPrivacyModal() {
    let modal = this.modal.create(PrivacyPolicyPage);
    modal.present();
  }

}
