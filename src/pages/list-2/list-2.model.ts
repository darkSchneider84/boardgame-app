
export class List2Model {
  tornei: Array<TorneoModel>;
}

export class TorneoModel {
  attributes: AttributeModel;
  relationships: Array<RelationModel> = [];
}

export class RelationModel {
  data:{
    id: number;
    type: string;
  }
}

export class AttributeModel {
  name: string;
  description: string;
  start_date: string;
}