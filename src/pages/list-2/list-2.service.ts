import { Injectable } from "@angular/core";
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { TorneoModel } from './list-2.model';

@Injectable()
export class List2Service {
  constructor(public http: Http) {}

  getData(token): Promise<TorneoModel[]> {

      return this.http.get('https://boardgametournament.beebeeboard.com/api/v1/workhours?access_token='+token)
       .toPromise()
       .then(response => response.json().data as TorneoModel[])
       .catch(this.handleError);
   
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
