import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';

import 'rxjs/Rx';

import { List2Model } from './list-2.model';
import { List2Service } from './list-2.service';

import { List1Page } from '../list-1/list-1';
import { Storage } from '@ionic/storage';
import * as moment from "moment";

@Component({
  selector: 'list-2-page',
  templateUrl: 'list-2.html'
})
export class List2Page {
  list2: List2Model = new List2Model();
  loading: any;
   token:any;

  constructor(
    public nav: NavController,
    public list2Service: List2Service,
    public loadingCtrl: LoadingController,
    public storage:Storage
  ) {
     storage.ready().then(() => {
        this.storage.get('token').then((token) => {
          this.token = token;
        });

      });
    this.loading = this.loadingCtrl.create();
  }

  ionViewDidLoad() {
    this.loading.present();

     this.storage.get('token').then((token) => {
        this.list2Service
          .getData(token)
          .then(data => {
        this.list2.tornei = data;
        this.loading.dismiss();
      });
    });
  }

  handleError() {
    console.log('error');
  }

  get_date(val) {
    return moment(val).format('YYYY MMM DD');  
  }


   goToClassifica(torneo_id) {
   
    this.nav.push(List1Page, {
      torneo_id: torneo_id
    });
  }

}
