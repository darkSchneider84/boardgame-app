import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import {Http, Headers, RequestOptions} from '@angular/http';
import { TabsNavigationPage } from '../tabs-navigation/tabs-navigation';
import { SignupPage } from '../signup/signup';
import { Storage } from '@ionic/storage';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';

@Component({
  selector: 'login-page',
  templateUrl: 'login.html'
})

export class LoginPage {
 
  username:string;
  pwd:string;
  login: FormGroup;
  main_page: { component: any };


  constructor(public nav: NavController, private http:Http, public storage: Storage) {
    this.login = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });

    storage.ready().then(() => {

      this.storage.get('token').then((token) => {
        console.log(token);
        if(token){
          console.log('entrat');
          this.nav.setRoot(this.main_page.component); 
        }
      });
      
    });
    this.main_page = { component: TabsNavigationPage };

    
  }

  doLogin(){
   
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization' :'Basic YWJjMTIzOnNzaC1zZWNyZXQ='
    });

    let options = new RequestOptions({
      headers: headers
    });

    this.login.value.grant_type= 'password';
    let data = JSON.stringify(this.login.value);
    
   this.http.post('https://boardgametournament.beebeeboard.com/api/oauth/token', data, options)
    .subscribe(response => {this.loginSuccess(response)},
    error => { this.handleError(); });

  }

  private loginSuccess(res): void {
     console.log(res);
     let token_string = JSON.parse(res._body);
     this.storage.set('token', token_string.access_token).then(() => {

      this.storage.set('username', this.login.value.username).then(() => {
        this.storage.set('password', this.login.value.password).then(() => {
             console.log('Token saved in storage');
             this.nav.setRoot(this.main_page.component); 
        });
      });

    });
     
  }

  handleError() {
    let imgEl: HTMLElement = document.getElementById('wronglogin');
    imgEl.style.display = 'block';
  }

  doFacebookLogin() {
    this.nav.setRoot(this.main_page.component);
  }

  doGoogleLogin() {
    this.nav.setRoot(this.main_page.component);
  }

  goToSignup() {
    this.nav.push(SignupPage);
  }

  goToForgotPassword() {
    this.nav.push(ForgotPasswordPage);
  }

}
