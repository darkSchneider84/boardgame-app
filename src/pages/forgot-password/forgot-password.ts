import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import {Http, Headers, RequestOptions} from '@angular/http';

import { TabsNavigationPage } from '../tabs-navigation/tabs-navigation';
import { LoginPage } from '../login/login';
import { SignupPage } from '../signup/signup';

@Component({
  selector: 'forgot-password-page',
  templateUrl: 'forgot-password.html'
})
export class ForgotPasswordPage {
  forgot_password: FormGroup;
  main_page: { component: any };
  login: { component: any};

  constructor(public nav: NavController, private http:Http) {
    this.main_page = { component: TabsNavigationPage };
    this.login = { component: LoginPage};

    this.forgot_password = new FormGroup({
      email: new FormControl('', Validators.required)
    });
  }

  recoverPassword(){
    console.log(this.forgot_password.value.email);

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    
    return this.http.get('https://boardgametournament.beebeeboard.com/api/v1/users/forgot?mail='+this.forgot_password.value.email, options)
    .toPromise()
    .then(response => {   
      let imgWr: HTMLElement = document.getElementById('wrongmail');
      imgWr.style.display = 'none';
      let imgEl: HTMLElement = document.getElementById('forgotmessage');
      imgEl.style.display = 'block';
    },this.handleError);

  }

  handleError(error) {
      let imgWr: HTMLElement = document.getElementById('wrongmail');
      imgWr.style.display = 'block';
      let imgEl: HTMLElement = document.getElementById('forgotmessage');
      imgEl.style.display = 'none';
  }

   goToSignup() {
    this.nav.push(SignupPage);
  }

  goToLogin() {
    this.nav.push(LoginPage);
  }

}
