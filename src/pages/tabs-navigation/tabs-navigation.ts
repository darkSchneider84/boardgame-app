import { Component } from '@angular/core';


import { SchedulePage } from '../schedule/schedule';
import { List2Page } from '../list-2/list-2';
import { ListingPage } from '../listing/listing';

@Component({
  selector: 'tabs-navigation',
  templateUrl: 'tabs-navigation.html'
})
export class TabsNavigationPage {
  tab1Root: any;
  tab2Root: any;
  tab3Root: any;

  constructor() {

    this.tab1Root = SchedulePage;
    this.tab2Root = List2Page;
    this.tab3Root = ListingPage;
  }
}
