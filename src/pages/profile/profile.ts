import { Component } from '@angular/core';
import { MenuController, SegmentButton, App, NavParams, LoadingController } from 'ionic-angular';

import { SettingsPage } from '../settings/settings';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/Rx';

import { ProfileService } from './profile.service';

@Component({
  selector: 'profile-page',
  templateUrl: 'profile.html'
})
export class ProfilePage {
  display: string;
  user: any;
  loading: any;
  token: any;

  constructor(
    public menu: MenuController,
    public app: App,
    public navParams: NavParams,
    public profileService: ProfileService,
    public loadingCtrl: LoadingController,
    private storage:Storage,private http:Http
  ) {
    this.display = "list";
    this.user = {};
    storage.ready().then(() => {+
      this.storage.get('token').then((token) => {
        this.token = token;
      });
    });
    this.loading = this.loadingCtrl.create();
  }

  ionViewDidLoad() {
    this.loading.present();
    this.storage.get('token').then((token) => {
      this.token = token;
      
         return this.http.get('https://boardgametournament.beebeeboard.com/api/v1/contacts/me?access_token='+token)
         .subscribe(response => {this.getSuccess(response)},
         error => { this.handleError(); });
     
     
    });
  }

  private getSuccess(res): void {
    let res_string = JSON.parse(res._body);

    this.user.name = res_string.data.attributes.name;
    this.user.image = "https://data.beebeeboard.com/"+res_string.data.attributes.thumbnail_url+"-small.jpeg";
    this.user.description = res_string.data.attributes.description;
    this.loading.dismiss();
  }

  handleError() {
    console.log('error');
  }

  goToSettings() {
    // close the menu when clicking a link from the menu
    this.menu.close();
    this.app.getRootNav().push(SettingsPage);
  }

  onSegmentChanged(segmentButton: SegmentButton) {
    // console.log('Segment changed to', segmentButton.value);
  }

  onSegmentSelected(segmentButton: SegmentButton) {
    // console.log('Segment selected', segmentButton.value);
  }
}
