import { Component } from '@angular/core';
import { NavController, SegmentButton, LoadingController,NavParams, App } from 'ionic-angular';
import 'rxjs/Rx';
import {Http} from '@angular/http';
import { ScheduleModel } from './schedule.model';
import { Storage } from '@ionic/storage';
import { ScheduleService } from './schedule.service';
import { EventsPage } from '../events/events';
import { List1Page } from '../list-1/list-1';
import { LoginPage } from '../login/login';
import { LogoutPage } from '../logout/logout';
import * as moment from "moment";

@Component({
  selector: 'schedule-page',
  templateUrl: 'schedule.html'
})
export class SchedulePage {
  segment: string;
  schedule: ScheduleModel = new ScheduleModel();
  loading: any;
  token: string;
  week: any;
  product_id:any;
  product_name:any;
  product_image:any;
  torneo_id:any;
  torneo_name:any;
  regolamento:any;

  constructor(
   public app: App,
    public nav: NavController,
    public scheduleService: ScheduleService,
    public loadingCtrl: LoadingController,
    private http:Http,
    public storage: Storage,
    public navParams: NavParams
  ) {
    this.product_id = navParams.get('product_id');
    this.torneo_id = navParams.get('torneo_id');
    this.product_name = navParams.get('product_name');
    this.product_image = navParams.get('product_image');
    this.torneo_name = navParams.get('torneo_name');
    this.regolamento = null;
    storage.ready().then(() => {
      this.storage.get('token').then((token) => {
        this.token = token;
      });
    });

    this.segment = "downcoming";
    this.loading = this.loadingCtrl.create();
   
    this.week ={};
  }

  ionViewDidLoad() {
    this.loading.present();

    this.storage.get('token').then((token) => {

        this.week.from = moment(new Date());
        this.http.get('https://boardgametournament.beebeeboard.com/api/v1/contacts/me?access_token='+token)
            .subscribe(contacts => {
              let contact = contacts.json();
              console.log(contact);
              if(contact){
                this.scheduleService
                .getData(token, this.week.from, this.product_id, this.torneo_id)
                .then(data => {
                  console.log(data);
                  
                  if(this.product_id){
                     this.http.get('https://boardgametournament.beebeeboard.com/api/v1/products/'+this.product_id+'?access_token='+this.token)
                    .subscribe(products => {
                      let product = products.json();

                      for(let included of product.included){
                        if(included.type == 'files'){
                          if(included.attributes.file_type == 'pdf'){
                            this.regolamento = "https://data.beebeeboard.com/"+included.attributes.file_url;
                          }
                        }
                      }
                    });
                  }

                  this.schedule.downcoming = data;
                  this.loading.dismiss();
                });
              }
              else
              {

              }
              
          },
          error => { this.loading.dismiss();this.handleError(); });

        
    });
  }


  handleError() {
    console.log('Ciao error');
    this.storage.set('token', null).then(() => {
      
        this.app.getRootNav().setRoot(LogoutPage);
    });

  }

  onSegmentChanged(segmentButton: SegmentButton) {
    console.log('Segment changed to', segmentButton.value);
    if(segmentButton.value == 'past'){
      this.week.from = this.week.from.add(-1,'months');
    }
    else
    {
      this.week.from = this.week.from.add(1,'months');
    }

    this.scheduleService
      .getData(this.token, this.week.from, this.product_id, this.torneo_id)
      .then(data => {
        console.log(data);
        this.segment = "downcoming";
        this.schedule.downcoming = data;
        this.loading.dismiss();
      });
  }

  onSegmentSelected(segmentButton: SegmentButton) {
    // console.log('Segment selected', segmentButton.value);
  }

  get_day(val) {
    return moment(val).format('DD');  
  }

  get_month(val) {
    return moment(val).format('MMM');  
  }

  get_time(val) {
    return moment(val).format('HH:mm');  
  }

  get_year(val) {
    return moment(val).format('YYYY');  
  }

  get_full(val) {
    return moment(val).format('DD/MM HH:mm');  
  }

  get_start_week(val) {
    return moment(val).startOf('week').format('DD-MM');  
  }

  get_month_year(val) {
    return moment(val).format('MMM YYYY');  
  }

  get_end_week(val) {
    return moment(val).endOf('week').format('DD-MM');  
  }

  goToEvent(id) {
    this.nav.push(EventsPage, {
      id: id
    });
  }

  goToClassificaProduct(product_id) {
    this.nav.push(List1Page, {
      product_id: product_id
    });
  }

   goToClassificaTorneo(torneo_id) {
    this.nav.push(List1Page, {
      torneo_id: torneo_id
    });
  }

}
