import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { EventModel} from './schedule.model';

@Injectable()
export class ScheduleService {
  constructor(public http: Http) {}

  getData(token, week, gioco, torneo): Promise<EventModel[]> {

    if(gioco == null && torneo == null){
       return this.http.get('https://boardgametournament.beebeeboard.com/api/v1/events?direction=asc&sort=start_date&from_date='+week.startOf('month').format('YYYY-MM-DD HH:mm:ss')+'&to_date='+week.endOf('month').format('YYYY-MM-DD HH:mm:ss')+'&access_token='+token)
       .toPromise()
       .then(response => response.json().data as EventModel[])
       .catch(this.handleError);
      
    }
    else if(gioco !== null)
    {
      return this.http.get('https://boardgametournament.beebeeboard.com/api/v1/events?product_id='+gioco+'&direction=asc&sort=start_date&access_token='+token)
       .toPromise()
       .then(response => response.json().data as EventModel[])
       .catch(this.handleError);
      
    }else if(torneo !== null)
    {
      return this.http.get('https://boardgametournament.beebeeboard.com/api/v1/events?workhour_id='+torneo+'&direction=asc&sort=start_date&access_token='+token)
       .toPromise()
       .then(response => response.json().data as EventModel[])
       .catch(this.handleError);
      
    }
     
   
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
    


}
