export class EventModel {
  attributes: AttributeModel;
  relationships: Array<RelationModel> = [];
}

export class RelationModel {
  data:{
    id: number;
    type: string;
  }
}

export class AttributeModel {
  title: string;
  start_date: string;
}


export class ScheduleModel {
  downcoming: Array<EventModel> = [];
  upcoming: Array<EventModel> = [];
}
