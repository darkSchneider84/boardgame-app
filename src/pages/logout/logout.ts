import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { WalkthroughPage } from '../walkthrough/walkthrough';

/*
  Generated class for the Projects page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-logout',
  templateUrl: 'logout.html'
})
export class LogoutPage {
	rootPage: any = WalkthroughPage;
  constructor(public nav: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    this.nav.setRoot(this.rootPage);
  }

}
