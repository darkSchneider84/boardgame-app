import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { SchedulePage } from '../schedule/schedule';
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';

import { ListingModel } from './listing.model';
import { ListingService } from './listing.service';


@Component({
  selector: 'listing-page',
  templateUrl: 'listing.html',
})

export class ListingPage {
  listing: ListingModel = new ListingModel();
  loading: any;
  token:any;

  constructor(
    public nav: NavController,
    public listingService: ListingService,
    public loadingCtrl: LoadingController,
    public storage:Storage
  ) {
     storage.ready().then(() => {
      this.storage.get('token').then((token) => {
        this.token = token;
      });

    });
    this.loading = this.loadingCtrl.create();
  }


  ionViewDidLoad() {
    this.loading.present();
     this.storage.get('token').then((token) => {
        this.listingService
          .getData(token)
          .then(data => {

            this.listing.giochi = data;
            this.listing.giochi_ok = [];

            for(let g of this.listing.giochi){
              g.attributes.image = "https://data.beebeeboard.com/"+g.attributes.thumbnail_url+"-small.jpeg";
              this.listing.giochi_ok.push(g);
            }

            console.log(this.listing.giochi_ok);
            this.loading.dismiss();
          });
      });
    }


  goToGioco(product_id: any, product_name: any, product_image:any) {
    this.nav.push(SchedulePage, {
      product_id: product_id,
      product_name: product_name,
      product_image: product_image
    });
  }

}
