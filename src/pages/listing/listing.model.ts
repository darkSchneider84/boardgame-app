export class ListingModel {
  giochi: Array<ProductModel>;
  giochi_ok: Array<ProductModel>;
}

export class ProductModel {
  attributes: AttributeModel;
  relationships: Array<RelationModel> = [];
}

export class RelationModel {
  data:{
    id: number;
    type: string;
  }
}

export class AttributeModel {
  name: string;
  thumbnail_url: string;
  image: string;
}


