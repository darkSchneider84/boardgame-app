import { Injectable } from "@angular/core";
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { ProductModel } from './listing.model';

@Injectable()
export class ListingService {
  constructor(public http: Http) {}

   getData(token): Promise<ProductModel[]> {

      return this.http.get('https://boardgametournament.beebeeboard.com/api/v1/products?access_token='+token)
       .toPromise()
       .then(response => response.json().data as ProductModel[])
       .catch(this.handleError);
   
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
