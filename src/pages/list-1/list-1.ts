import { Component } from '@angular/core';
import { NavController, NavParams,  LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import * as moment from "moment";

import 'rxjs/Rx';

import { List1Model } from './list-1.model';

@Component({
  selector: 'list-1-page',
  templateUrl: 'list-1.html'
})
export class List1Page {
  list1: List1Model = new List1Model();
  loading: any;
  product_id: any;
  torneo_id: any;
  torneo_name: any;
  product_name: any;
  giocatore_name: any;
  private id: any;
  final : any;
  final1 : any;
  giocatori: any;
  giochi: any;
  new_project: any;
  scorer_id: any;
  personal_score: boolean;
  giocatore_id: any;

 
  token: string;
  constructor(
    public navCtrl: NavController, public navParams: NavParams, 
    private storage:Storage,private http:Http,public loadingCtrl: LoadingController,
  ) {
    this.id = navParams.get('id');
    
    this.final = [];
    this.final1 = [];
    this.giocatori = [];
    this.giochi = [];
    this.new_project = {};
    this.new_project.data ={};
    this.new_project.data.attributes = {};
    this.product_id = navParams.get('product_id');
    this.torneo_id = navParams.get('torneo_id');
    this.loading = this.loadingCtrl.create();
    this.personal_score = navParams.get('personal_score'); 
    this.giocatore_id = navParams.get('giocatore_id');
    this.torneo_name = '';
    this.giocatore_name = '';
    this.product_name = '';

    storage.ready().then(() => {+
      this.storage.get('token').then((token) => {
        this.token = token;
      });
    });

  }

  ionViewDidLoad() {
    this.loading.present();
   this.storage.get('token').then((token) => {
      this.token = token;

      if(this.product_id){
         return this.http.get('https://boardgametournament.beebeeboard.com/api/v1/projects?xpag=10000&product_id='+this.product_id+'&access_token='+token)
         .subscribe(response => {this.getSuccess(response)},
         error => { this.handleError(); });
      }
      else if(this.giocatore_id){
       return this.http.get('https://boardgametournament.beebeeboard.com/api/v1/projects?xpag=10000&workhour_id='+this.torneo_id+'&contact_id='+this.giocatore_id+'&access_token='+token)
         .subscribe(response => {this.getSuccess(response)},
         error => { this.handleError(); });
      }
      else if(this.torneo_id){
       return this.http.get('https://boardgametournament.beebeeboard.com/api/v1/projects?xpag=10000&workhour_id='+this.torneo_id+'&access_token='+token)
         .subscribe(response => {this.getSuccess(response)},
         error => { this.handleError(); });
      }
      
     
    });
  }

  private getSuccess(res): void {
    if(this.product_id){
      let res_string = JSON.parse(res._body);
      let totals = 0;

      this.http.get('https://boardgametournament.beebeeboard.com/api/v1/products/'+this.product_id+'?access_token='+this.token)
      .subscribe(products => {
        let product = products.json();

        this.product_name = product.data.attributes.name;
           
      });

      if(res_string.data.length == 0){
        this.loading.dismiss(); 
      }

      for(let included of res_string.included){
          if(included.type == 'contacts'){
            console.log(included);
            this.giocatori[included.id] = {
              'punti':0,
              'posizione':1,
              'primi_posti':0,
              'secondi_posti':0,
              'terzi_posti':0,
              'image': "https://data.beebeeboard.com/"+included.attributes.thumbnail_url+"-small.jpeg",
              'name': included.attributes.name,
              'contact_id': included.attributes.id
            };
          }
        }

      console.log('giocatori:');
      console.log(this.giocatori);

      for(let punteggio of res_string.data){
           this.http.get('https://boardgametournament.beebeeboard.com/api/v1/projects/'+punteggio.id+'?access_token='+this.token)
         .subscribe(projects => {
            let project = projects.json();
            let contact_id = 0;

            for(let included of project.included){
              if(included.type == 'contacts'){
                contact_id = included.attributes.id;
              }
            }

            for(let included of project.included){
              if(included.type == 'custom_fields'){
                if(included.attributes.organization_custom_field_id == 315){
                  this.giocatori[contact_id].punti += included.attributes.number_value;
                }
                if(included.attributes.organization_custom_field_id == 314){
                  if(included.attributes.number_value == 1){
                      this.giocatori[contact_id].primi_posti++;
                  }
                  else if(included.attributes.number_value == 2){
                     this.giocatori[contact_id].secondi_posti++;
                  }
                  else if(included.attributes.number_value == 3){
                     this.giocatori[contact_id].terzi_posti++;
                  }
                  
                }
              }
            }
          
           if(totals++ >= res_string.data.length-1){
            
            
            for(let included of res_string.included){
              if(included.type == 'contacts'){
                this.final.push(this.giocatori[included.id]);
              }
            }
            console.log(this.final);
            this.final.sort(function(a,b){
              return parseFloat(b.punti) - parseFloat(a.punti);
            });
            this.loading.dismiss();
           }

         },
         error => { this.handleError(); }); 
      }

    }
    else if(this.giocatore_id){
      this.http.get('https://boardgametournament.beebeeboard.com/api/v1/contacts/'+this.giocatore_id+'?access_token='+this.token)
      .subscribe(contacts => {
        let contact = contacts.json();

        this.giocatore_name = contact.data.attributes.name;
           
      });

       this.http.get('https://boardgametournament.beebeeboard.com/api/v1/workhours/'+this.torneo_id+'?access_token='+this.token)
      .subscribe(workhours => {
        let torneo = workhours.json();

        this.torneo_name = torneo.data.attributes.text;
           
      });
      let res_string = JSON.parse(res._body);
      let totals = 0;

      if(res_string.data.length == 0){
        this.loading.dismiss(); 
      }


      for(let punteggio of res_string.data){

         this.http.get('https://boardgametournament.beebeeboard.com/api/v1/projects/'+punteggio.id+'?access_token='+this.token)
         .subscribe(projects => {
            let project = projects.json();
            this.giochi[project.data.attributes.id] = {
              'date': project.data.attributes.start_date,
              'punti':0,
              'posizione':1,
              'name':'',
              'image': '',
              'product_id':0
            };
            console.log(project.included);
            for(let included of project.included){
              if(included.type == 'custom_fields'){
                if(included.attributes.organization_custom_field_id == 315){
                  this.giochi[project.data.attributes.id].punti = included.attributes.number_value;
                }
                if(included.attributes.organization_custom_field_id == 314){
                      this.giochi[project.data.attributes.id].posizione = included.attributes.number_value;
                  
                }
              }

              if(included.type == 'products'){
                this.giochi[project.data.attributes.id].name = included.attributes.name;
                this.giochi[project.data.attributes.id].image = "https://data.beebeeboard.com/"+included.attributes.thumbnail_url+"-small.jpeg";
                this.giochi[project.data.attributes.id].product_id= included.attributes.id;
              }
            }
          
            
            if(totals++ >= res_string.data.length-1){
               for(let proj of res_string.data){
                 
                    this.final1.push(this.giochi[proj.attributes.id]);
                  
                }
              console.log(this.final1);
               this.loading.dismiss();
            }


         },
         error => { this.handleError(); }); 
      }
    }else if(this.torneo_id){

      let res_string = JSON.parse(res._body);
      let totals = 0;

      this.http.get('https://boardgametournament.beebeeboard.com/api/v1/workhours/'+this.torneo_id+'?access_token='+this.token)
         .subscribe(workhours => {
            let workhour = workhours.json();
            this.torneo_name = workhour.data.attributes.text;
            for(let included of workhour.included){
              if(included.type == 'contacts'){
                this.giocatori[included.id] = {
                  'punti':0,
                  'posizione':1,
                  'primi_posti':0,
                  'secondi_posti':0,
                  'terzi_posti':0,
                  'image': "https://data.beebeeboard.com/"+included.attributes.thumbnail_url+"-small.jpeg",
                  'name': included.attributes.name,
                  'contact_id': included.attributes.id
                };
              }
            }

          for(let punteggio of res_string.data){
               this.http.get('https://boardgametournament.beebeeboard.com/api/v1/projects/'+punteggio.id+'?access_token='+this.token)
             .subscribe(projects => {
                let project = projects.json();
                let contact_id = 0;

                for(let included of project.included){
                  if(included.type == 'contacts'){
                    contact_id = included.attributes.id;
                  }
                }

                for(let included of project.included){
                  if(included.type == 'custom_fields'){
                    if(included.attributes.organization_custom_field_id == 315){
                      this.giocatori[contact_id].punti += included.attributes.number_value;
                    }
                    if(included.attributes.organization_custom_field_id == 314){
                      if(included.attributes.number_value == 1){
                          this.giocatori[contact_id].primi_posti++;
                      }
                      else if(included.attributes.number_value == 2){
                         this.giocatori[contact_id].secondi_posti++;
                      }
                      else if(included.attributes.number_value == 3){
                         this.giocatori[contact_id].terzi_posti++;
                      }
                      
                    }
                  }
                }
              
               if(totals++ >= res_string.data.length-1){
                
                
                for(let included of workhour.included){
                  if(included.type == 'contacts'){
                    this.final.push(this.giocatori[included.id]);
                  }
                }

                this.final.sort(function(a,b){
                  return parseFloat(b.punti) - parseFloat(a.punti);
                });
                this.loading.dismiss();
               }

             },
             error => { this.handleError(); }); 
          }

           
         },
         error => { this.handleError(); });
     
    }
   }

   handleError() {
    console.log('error');
  }

   goToClassifica(giocatore_id) {
    this.navCtrl.push(List1Page, {
      giocatore_id: giocatore_id,
      torneo_id: this.torneo_id,
      personal_score: true
    });
  }

  get_full(val) {
    return moment(val).format('DD/MM HH:mm');  
  }

  get_day(val) {
    return moment(val).format('DD');  
  }

  get_month(val) {
    return moment(val).format('MMM');  
  }

  get_time(val) {
    return moment(val).format('HH:mm');  
  }

  get_year(val) {
    return moment(val).format('YYYY');  
  }

}
