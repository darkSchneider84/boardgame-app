

export class ListModel {
  id: number;
  name: string;
  image: string;
  posizione: number;
  punti: number;
}

export class List1Model {
  items: Array<ListModel>;
}
