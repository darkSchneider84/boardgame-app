export class PositionModel {
  id: number;
  contact_status_id: number;
  name: string;
  image: string;
  posizione: number;
  punti: number;
}

export class ClassificaModel {
  items: Array<PositionModel>;
}