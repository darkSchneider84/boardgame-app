import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ClassificaModel, PositionModel} from './events.model';
import { Http } from '@angular/http';
import { FormGroup, FormControl } from '@angular/forms';
import { SchedulePage } from '../schedule/schedule';
import { counterRangeValidator } from '../../components/counter-input/counter-input';
import * as moment from "moment";
import 'rxjs/Rx';
 
/*
  Generated class for the Events page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/


@Component({
  selector: 'page-events',
  templateUrl: 'events.html'
})


export class EventsPage {
  punteggi: boolean;
  product_id: any;
  torneo_id: any;
  private id: any;
  event: any;
  classifica: ClassificaModel = new ClassificaModel();
  final : any;
  savePoints: any;
  giocatori: any;
  counterForm: any;
  new_project: any;

 
  token: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage:Storage,private http:Http) {
  	this.id = navParams.get('id');
    
  	this.event = {};
  	this.punteggi = false;
  	this.final = [];
    this.giocatori = [];
    this.savePoints = false;
    this.new_project = {};
    this.new_project.data ={};
    this.new_project.data.attributes = {};
    this.product_id = 0;
    this.torneo_id = 0;

    this.counterForm = new FormGroup({
      counter: new FormControl(1, counterRangeValidator(7, 1))
    });

  	storage.ready().then(() => {+
  	  this.storage.get('token').then((token) => {
        this.token = token;
      });
    });

  }

  ionViewDidLoad() {

  	this.storage.get('token').then((token) => {
  		this.token = token;
	  	return this.http.get('https://boardgametournament.beebeeboard.com/api/v1/events/'+this.id+'?access_token='+token)
	       .subscribe(response => {this.getSuccess(response)},
	   		 error => { this.handleError(); });
	  });
    
  }

   private getSuccess(res): void {
     let res_string = JSON.parse(res._body);
     console.log(res_string.data.attributes);
     
     this.event.description = res_string.data.attributes.description;

     for(let included of res_string.included){
     
      	if(included.type == 'products'){
          this.product_id = included.attributes.id;
      		this.event.image = "https://data.beebeeboard.com/"+included.attributes.thumbnail_url+"-medium.jpeg";
      		this.event.title = included.attributes.name;
      	}

        if(included.type == 'contacts'){
            let person: PositionModel = new PositionModel();

            person.name = included.attributes.name;
            person.id = included.attributes.id;
            person.contact_status_id = included.attributes.contact_status_id;
            this.giocatori.push(person);
        }

        if(included.type == 'workhours'){
            
            this.torneo_id = included.attributes.id;
        }

      	if(included.type == 'projects'){
      		this.punteggi = true;
      	}

     }

     this.final = [];

     for(let included of res_string.included){
     	if(included.type == 'projects'){
      		this.http.get('https://boardgametournament.beebeeboard.com/api/v1/projects/'+included.attributes.id+'?access_token='+this.token)
	       .subscribe(projects => {
	       		let project = projects.json();
	       		let contact_id = 0;

	       		

	       		for(let included of project.included){
	       			if(included.type == 'contacts'){
	       				let person: PositionModel = new PositionModel();

	       				person.name = included.attributes.name;
	       				person.image =  "https://data.beebeeboard.com/"+included.attributes.thumbnail_url+"-small.jpeg";
	       				this.classifica[included.attributes.id] = person;
	       				contact_id = included.attributes.id;
	       			}
	       		}

	       		for(let included of project.included){
	       			if(included.type == 'custom_fields'){
	       				if(included.attributes.organization_custom_field_id == 314){
	       					this.classifica[contact_id].posizione = included.attributes.number_value;
	       				}
	       				if(included.attributes.organization_custom_field_id == 315){
	       					this.classifica[contact_id].punti = included.attributes.number_value;
	       					
	       				}
	       			}
	       		}

	     this.final.push(this.classifica[contact_id]);

				this.final.sort(function(a,b){
					return parseFloat(a.posizione) - parseFloat(b.posizione);
				});

				console.log(this.final);
	       },
	   		 error => { this.handleError(); });
      		
      	}
     }
  }

  handleError() {
   	console.log('error');
  }

  salvaPunti() {
    this.savePoints = true;
  }

  salvaPunteggi() {
    let players = this.giocatori.length;
    for(let giocatore of this.giocatori){
      let val = document.getElementById('punti_'+giocatore.id);
      let posizione :number = parseInt(val.childNodes[2].textContent);
      
      let new_custom_fields = [];

      this.http.get('https://boardgametournament.beebeeboard.com/api/v1/products/'+this.product_id+'?access_token='+this.token)
         .subscribe(products => {
            let product = products.json();
            let product_points = 0;
            let step_points = 0;

            
            for(let included of product.included){
              if(included.type == 'custom_fields'){
                if(included.attributes.organization_custom_field_id == 312){
                  product_points = included.attributes.number_value;
                }
                if(included.attributes.organization_custom_field_id == 313){
                  step_points = included.attributes.number_value;
                }
              }
            }
            



            let custom_field = {
                'data':{
                  'attributes':{
                    'value':null,
                    'number_value':posizione,
                    'boolean_value':null
                  },
                      'relationships':{
                        'organization_custom_field':{
                          'data':
                            {
                              'type':'organization_custom_fields',
                              'id':314
                            }
                        }
                      }
                }
              };

           this.http.post('https://boardgametournament.beebeeboard.com/api/v1/custom_fields?access_token='+this.token, custom_field)
            .subscribe(response => {
              console.log('custom_salvato');
              let proj = response.json();
              new_custom_fields.push(proj.data.id);


              let custom_field = {
                    'data':{
                      'attributes':{
                        'value':null,
                        'number_value': (product_points + (step_points * (players - posizione))),
                        'boolean_value':null
                      },
                      'relationships':{
                        'organization_custom_field':{
                          'data':
                            {
                              'type':'organization_custom_fields',
                              'id':315
                            }
                        }
                      }
                    }

                  };


               this.http.post('https://boardgametournament.beebeeboard.com/api/v1/custom_fields?access_token='+this.token, custom_field)
                .subscribe(response => {
                  console.log('custom_salvato');
                  let proj = response.json();
                  let new_custom_fields_array =[];
              

                  new_custom_fields.push(proj.data.id);

                  for(let cf of new_custom_fields){
                    new_custom_fields_array.push({
                      'type':'custom_field',
                      'id':cf
                    });
                  }
                  
                  this.new_project.data.attributes.name = posizione + ' ' +giocatore.name;
                  this.new_project.data.attributes.start_date = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
                  this.new_project.data.attributes.description = '';
                  this.new_project.data.attributes.price = null;
                  this.new_project.data.attributes.cost = null;
                  this.new_project.data.attributes.duration = null;
                  this.new_project.data.attributes.archivied = false;
                  this.new_project.data.relationships = {
                     'contacts': {
                         'data': [{
                             'id': giocatore.id,
                             'type': 'contacts'
                         }]
                     },
                     'products':{
                      'data':[{
                        'id': this.product_id,
                        'type': 'products'
                      }]
                     },
                     'custom_fields':{
                      'data': new_custom_fields_array
                     }
                  };


                   this.http.post('https://boardgametournament.beebeeboard.com/api/v1/projects?access_token='+this.token, this.new_project)
                    .subscribe(response => {
                        let proj = response.json();
                        
                        let project_id = proj.data.id;
                        let relation = {
                          'operation':'add',
                          'data':[{
                            'name':'event',
                            'id':this.id
                          },{
                            'name':'project',
                            'id':project_id 
                          }
                          ]
                        };

                         this.http.post('https://boardgametournament.beebeeboard.com/api/v1/relations?access_token='+this.token, relation)
                          .subscribe(response => {
                              
                          },
                          error => { this.handleError(); });

                        let relation_work = {
                          'operation':'add',
                          'data':[{
                            'name':'workhour',
                            'id':this.torneo_id
                          },{
                            'name':'project',
                            'id':project_id 
                          }
                          ]
                        };
                        this.http.post('https://boardgametournament.beebeeboard.com/api/v1/relations?access_token='+this.token, relation_work)
                          .subscribe(response => {
                              
                          },
                          error => { this.handleError(); });

                    },
                    error => { this.handleError(); });
                 
                },
                error => { this.handleError(); }); 
             
            },
            error => { this.handleError(); });
            
         },
         error => { this.handleError(); });

    }
     this.savePoints = false;
     this.punteggi = false;
     this.navCtrl.push(SchedulePage);
  }

}
